use itertools::Itertools;
use std::collections::HashMap;
use std::io::{BufRead, BufReader, Read, stdin};

fn main() {
    solve(stdin());
}

fn parse(input: impl Read) -> (HashMap<String, f64>, String) {
    let stream = BufReader::new(input);
    let mut lines = stream.lines().flatten();
    let n = lines.next().unwrap().trim().parse::<usize>().unwrap();
    let mut resistances = HashMap::new();
    for _ in 0..n {
        let line = lines.next().unwrap();
        let (name, value) = line.split_whitespace().tuples().next().unwrap();
        resistances.insert(name.to_string(), value.parse::<f64>().unwrap());
    }
    let circuit = lines.next().unwrap();
    (resistances, circuit)
}

fn branch<'a>(
    circuit: &mut impl Iterator<Item = &'a str>,
    resistances: &HashMap<String, f64>,
) -> Option<f64> {
    let tok = circuit.next().unwrap();
    match tok {
        "(" => {
            let mut acc = 0f64;
            while let Some(value) = branch(circuit, resistances) {
                acc += value;
            }
            Some(acc)
        }
        "[" => {
            let mut acc = 0f64;
            while let Some(value) = branch(circuit, resistances) {
                acc += value.recip();
            }
            Some(acc.recip())
        }
        "]" | ")" => None,
        tok => Some(resistances[tok]),
    }
}

fn solve(input: impl Read) -> f64 {
    let (resistances, circuit) = parse(input);
    let res = branch(&mut circuit.split_whitespace(), &resistances).unwrap();
    (res * 10f64).round() / 10f64
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample() {
        let input = " 3
        A 24
        B 8
        C 48
        [ ( A B ) [ C A ] ]";
        assert_eq!(solve(input.as_bytes()), 10.7);
    }
}
