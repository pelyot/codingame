use std::io::{stdin, BufRead, BufReader, Read};

fn main() {
    solve(stdin());
}

fn solve(input: impl Read) {
    let reader = BufReader::new(input);
    let mut stream = reader.lines().flatten().flat_map(|l| {
        l.split_ascii_whitespace()
            .map(|w| w.trim().parse::<usize>().unwrap())
            .collect::<Vec<_>>()
            .into_iter()
    });
    let n_devices = stream.next().unwrap();
    let n_clicks = stream.next().unwrap();
    let capacity = stream.next().unwrap();
    let consumption = (0..n_devices).map(|_| stream.next().unwrap()).collect::<Vec<_>>();
    let sequence = stream.take(n_clicks).collect::<Vec<_>>();
    let mut state = vec![false; n_devices];
    let mut max_current = 0;
    let mut current = 0;
    for step in sequence {
        if state[step - 1] {
            current -= consumption[step - 1];
        } else {
            current += consumption[step - 1];
        }
        state[step - 1] ^= true;
        max_current = std::cmp::max(current, max_current);
        if current > capacity {
            println!("Fuse was blown.");
            return;
        }
    }
    println!("Fuse was not blown.");
    println!("Maximal consumed current was {} A.", max_current);
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample() {
        let input = "5 2 10
        11 6 11 10 10
        3 3";
        solve(input.as_bytes());
    }
}
