use itertools::Itertools;
use std::io::{stdin, BufRead, BufReader, Read};

fn main() {
    print(solve(parse(stdin())));
}

type Section = (usize, usize);
struct Input {
    length: usize,
    sections: Vec<Section>,
}

#[derive(Debug, PartialEq)]
enum Solution {
    AllPainted,
    Sections(Vec<Section>),
}

fn parse(input: impl Read) -> Input {
    let reader = BufReader::new(input);
    let mut lines = reader.lines().flatten();
    let length = lines.next().unwrap().trim().parse::<usize>().unwrap();
    let nb_sections = lines.next().unwrap().trim().parse::<usize>().unwrap();
    let mut sections = Vec::new();
    for _ in 0..nb_sections {
        sections.push(
            lines
                .next()
                .unwrap()
                .split_whitespace()
                .map(|s| s.parse().unwrap())
                .tuples()
                .next()
                .unwrap(),
        );
    }
    Input { length, sections }
}

fn solve(mut input: Input) -> Solution {
    input.sections.sort_unstable();
    let mut result = Vec::new();
    let mut position = 0;
    for s in input.sections {
        if position < s.0 {
            result.push((position, s.0));
        }
        position = std::cmp::max(position, s.1);
    }
    if position < input.length {
        result.push((position, input.length));
    }
    if result.is_empty() {
        Solution::AllPainted
    } else {
        Solution::Sections(result)
    }
}

fn print(solution: Solution) {
    if let Solution::Sections(result) = solution {
        for r in &result {
            println!("{} {}", r.0, r.1);
        }
    } else {
        println!("All painted")
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn sample() {
        let input = "10
        2
        1 4
        5 6";
        assert_eq!(
            Solution::Sections(vec![(0, 1), (4, 5), (6, 10)]),
            solve(parse(input.as_bytes()))
        );
    }

    #[test]
    fn overlapping() {
        let input = "10
        2
        1 4
        3 5";
        assert_eq!(
            Solution::Sections(vec![(0, 1), (5, 10)]),
            solve(parse(input.as_bytes()))
        );
    }

    #[test]
    fn all_painted() {
        let input = "12
        5
        6 10
        0 4
        7 8
        3 7
        8 12";
        assert_eq!(Solution::AllPainted, solve(parse(input.as_bytes())));
    }
}
