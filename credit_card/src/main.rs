use std::io::{stdin, BufRead, BufReader, Read};

type Input = Vec<Vec<u32>>;

fn solve(input: Input) -> Vec<bool> {
    input
        .iter()
        .map(|digits| {
            let even: u32 = digits
                .iter()
                .step_by(2)
                .map(|d| {
                    let dd = d * 2;
                    if dd >= 10 {
                        dd - 9
                    } else {
                        dd
                    }
                })
                .sum();
            let odd: u32 = digits.iter().skip(1).step_by(2).sum();
            (even + odd) % 10 == 0
        })
        .collect()
}

fn main() {
    for r in solve(parse(stdin())) {
        println!("{}", if r { "YES" } else { "NO" });
    }
}

fn parse(input: impl Read) -> Input {
    let reader = BufReader::new(input);
    let mut lines = reader.lines().flatten();
    let n = lines.next().unwrap().parse::<usize>().unwrap();
    (0..n)
        .map(|_| {
            let line = lines.next().unwrap();
            line.chars()
                .flat_map(|c| c.to_digit(10))
                .take(16)
                .collect()
        })
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample() {
        let input = "2
        4556 7375 8689 9855
        4024 0071 0902 2143";
        assert_eq!(vec![true, false], solve(parse(input.as_bytes())));
    }
}
