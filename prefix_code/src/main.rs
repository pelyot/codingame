use std::io::{stdin, BufRead, BufReader, Read};

fn main() {
    println!("{}", solve(parse(stdin())));
}

struct Input {
    dict: Vec<(String, char)>,
    message: String,
}

fn parse(input: impl Read) -> Input {
    let mut lines = BufReader::new(input).lines().flatten();
    let n = lines.next().unwrap().parse::<usize>().unwrap();
    let mut dict = Vec::new();
    for _ in 0..n {
        let line = lines.next().unwrap();
        let mut toks = line.split_whitespace();
        dict.push((
            toks.next().unwrap().to_string(),
            toks.next().unwrap().parse::<u8>().unwrap() as char,
        ));
    }
    let message = lines.next().unwrap().trim().to_string();
    Input { dict, message }
}

fn solve(input: Input) -> String {
    let mut result = String::new();
    let mut remain = &input.message[..];
    let mut index = 0;
    while !remain.is_empty() {
        if let Some((code, decode)) = input
            .dict
            .iter()
            .find(|(code, _)| remain.starts_with(code.as_str()))
        {
            remain = &remain[code.len()..];
            index += code.len();
            result.push(*decode);
        } else {
            return format!("DECODE FAIL AT INDEX {}", index);
        }
    }
    result
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test() {
        let input = "5
        1 97
        001 98
        000 114
        011 99
        010 100
        10010001011101010010001";
        assert_eq!(solve(parse(input.as_bytes())), "abracadabra");
    }
}
