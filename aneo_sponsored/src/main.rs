use itertools::Itertools;
use std::io::{stdin, BufRead, BufReader, Read};
use std::str::FromStr;

fn main() {
    println!("{}", solve(parse(stdin())));
}

type Distance = f64;
type MaxSpeed = f64;
type Duration = f64;
struct Input { max_speed : MaxSpeed, signals: Vec<(Distance, Duration)> }

fn parse(input: impl Read) -> Input {
    let buf = BufReader::new(input);
    let mut lines = buf.lines().flatten();
    let max_speed = lines.next().unwrap().parse::<MaxSpeed>().unwrap() as f64;
    let signals = lines
        .skip(1)
        .map(|l| {
            l.split_whitespace()
                .map(f64::from_str)
                .flatten()
                .tuples()
                .next()
                .unwrap()
        })
        .collect();
    Input {max_speed, signals}
}

fn solve(input: Input) -> usize {
    let mut speed = input.max_speed; 
    while speed > 0f64 {
        let mut red = false;
        for sig in &input.signals {
            let time = sig.0 / speed * 3.6; // convert speed to m/s
            if time % (2f64 * sig.1) >= sig.1 {
                red = true;
                break;
            }
        }
        if red == false {
            break;
        }
        speed -= 1f64;
    }
    speed.trunc() as usize
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn german_highway() {
        let input = "200
        6
        1000 15
        3000 10
        4000 30
        5000 30
        6000 5
        7000 10";
        assert_eq!(solve(parse(input.as_bytes())), 60);
    }
    #[test]
    fn test() {
        let input = "80
        4
        700 25
        2200 15
        3000 10
        4000 28";
        assert_eq!(solve(parse(input.as_bytes())), 49);
    }
}
