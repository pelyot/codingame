use std::io::{Read, BufRead, BufReader, stdin};

fn main() {
    print(&solve(Input::parse(stdin())));
}

fn solve(input: Input) -> Vec<Vec<char>> {
    let angle = input.angle % 360;
    let rect = if angle == 45 {
        input.rect
    } else if angle == 135 {
        rotate90(input.rect)
    } else if angle == 225 {
        rotate180(input.rect)
    } else {
        rotate270(input.rect)
    };
    let output_size = rect.len() * 2 - 1;
    let mut output = vec![vec![' '; output_size]; output_size];
    let n = input.n;
    let mut start_r = 0;
    let mut start_c = n - 1;
    let mut start_out_c = output_size / 2 ;
    let mut start_out_c_inc = false;
    let mut out_r = 0;
    for _ in 0..(2 * n) {
        let mut r = start_r;
        let mut c = start_c;
        let mut out_c = start_out_c;
        while r < n && r >= 0 && c < n && c >= 0 {
            output[out_r][out_c] = rect[r as usize][c as usize];
            r += 1;
            c += 1;
            out_c += 2;
        }
        if start_c > 0 {
            start_c -= 1;
        } else {
            start_r += 1;
        }
        out_r += 1;
        if start_out_c == 0 {
            start_out_c_inc = true;
        }
        if start_out_c_inc {
            start_out_c += 1;
        } else {
            start_out_c -= 1;
        }
    }
    output
}


struct Input {
    n: i32,
    rect: Vec<Vec<char>>,
    angle: usize
}
impl Input {
    fn parse(input : impl Read) -> Input {
        let reader = BufReader::new(input);
        let mut lines = reader.lines().flatten();
        let n = lines.next().unwrap().trim().parse::<i32>().unwrap();
        let angle = lines.next().unwrap().trim().parse::<usize>().unwrap();
        let rect = lines
            .filter(|s| !s.is_empty())
            .take(n as usize)
            .map(|l| l.trim().chars().filter(|c| !c.is_whitespace()).collect())
            .collect();
        Input { n, rect, angle }
    }
}

fn print(output: &Vec<Vec<char>>) {
    for row in output {
        println!("{}", row.iter().collect::<String>());
    }
}

fn rotate270(rect: Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut rev : Vec<Vec<char>> = rect.iter().rev().cloned().collect();
    for i in 0..rev.len() {
        for j in 0..i {
            let tmp = rev[i][j];
            rev[i][j] = rev[j][i];
            rev[j][i] = tmp;
        }
    }
    rev
}
fn rotate180(rect: Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut r = rotate270(rect);
    r = rotate270(r);
    r
}
fn rotate90(rect: Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut r = rotate270(rect);
    r = rotate270(r);
    r = rotate270(r);
    r
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample() {
        let input = "5
        45
        # # # # #
        # - . . #
        # # - . #
        # # # - #
        # # # # #";
        print(&solve(Input::parse(input.as_bytes())));
    }

    #[test]
    fn sample1() {
        let input = "2
        45
        1 2
        3 4";
        print(&solve(Input::parse(input.as_bytes())));
    }

    #[test]
    fn rotate90_test() {
        let input = "5
        45
        # # # # #
        # - . . #
        # # - . #
        # # # - #
        # # # # #";
        let i = Input::parse(input.as_bytes());
        let rotated = rotate180(i.rect);
        print(&rotated);
    }
}