use std::io::{stdin, Read};
use std::num::ParseIntError;
use std::str::FromStr;

fn main() {
    let mut input = String::new();
    stdin().read_to_string(&mut input).unwrap();
    let grid = Grid::from_str(&input).unwrap();
    println!("{:?}", grid.check());
}

struct Grid {
    data: Vec<u8>,
}

impl Grid {
    fn check(&self) -> bool {
        let mut result = true;
        for i in 0..9 {
            result = result && self.check_row(i);
            result = result && self.check_col(i);
        }
        for ri in 0..3 {
            for ci in 0..3 {
                result = result && self.check_square(ri, ci);
            }
        }
        result
    }
    fn check_row(&self, r: usize) -> bool {
        self.data[9 * r..9 * (r + 1)]
            .iter()
            .fold(0, |acc, e| acc | (1 << e))
            == 0b111_111_111_0
    }
    fn check_col(&self, c: usize) -> bool {
        self.data
            .iter()
            .skip(c)
            .step_by(9)
            .fold(0, |acc, e| acc | (1 << e))
            == 0b111_111_111_0
    }
    fn check_square(&self, r: usize, c: usize) -> bool {
        let mut result = 0;
        for ri in 0..3 {
            for ci in 0..3 {
                let row = 3 * r + ri;
                let col = 3 * c + ci;
                result |= 1 << self.data[9 * row + col];
            }
        }
        result == 0b111_111_111_0
    }
}
impl FromStr for Grid {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, <Self as FromStr>::Err> {
        Ok(Grid {
            data: s
                .split_whitespace()
                .map(|e| u8::from_str(e).unwrap())
                .collect(),
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test() {
        let input = "\
        1 2 3 4 5 6 7 8 9
        4 5 6 7 8 9 1 2 3
        7 8 9 1 2 3 4 5 6
        9 1 2 3 4 5 6 7 8
        3 4 5 6 7 8 9 1 2
        6 7 8 9 1 2 3 4 5
        8 9 1 2 3 4 5 6 7
        2 3 4 5 6 7 8 9 1
        5 6 7 8 9 1 2 3 4";
        let g = Grid::from_str(&input).unwrap();
        assert!(g.check() == true);
    }
}
